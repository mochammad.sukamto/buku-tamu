<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTglDatangToFormTamu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_tamu', function (Blueprint $table) {
            $table->dateTime('tanggal_datang')->nullable();
            $table->dateTime('tanggal_keluar')->nullable();
            $table->text('barang_titip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_tamu', function (Blueprint $table) {
            $table->dropColumn('[tanggal_datang]');
            $table->dropColumn('[tanggal_keluar]');
            $table->dropColumn('[barang_titip]');
        });
    }
}
