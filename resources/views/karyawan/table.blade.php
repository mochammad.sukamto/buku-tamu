@extends('adminlte.master')

@section('content')
    
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h1 class="card-title">Table Karyawan</h1>
            </div>
            
            <!-- /.card-header -->
            <div class="card-body">
              <div class="card-navbar col-xs-4 text-right">
                <a href="{{url('karyawan/create')}}" class="btn btn-info">Add+</a>
              </div><br><br>
              <div class="card-navbar col-xs-4 text-right">
                <button onclick="ExportExcel()" class="btn btn-info">Excel+</button>
              </div><br><br>
              <div id="example1_filter" class="dataTables_filter">
                <label>Search: <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example1">
                </label>
              </div>
              <table id="html_table" class="table table-bordered table-hover downloadExcel">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Password</th>
                  <th>No Telpon</th>
                  <th>Alamat</th>
                  <th class="excludeThisClass">Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($table as $key => $table)
                      <tr>
                        <td> {{$key + 1}} </td>
                        <td> {{$table->name}} </td>
                        <td> {{$table->password}} </td>
                        <td> {{$table->no_telpon}} </td>
                        <td> {{$table->alamat}} </td>
                        <td class="excludeThisClass" > <a href="/karyawan/edit/{{$table->id}}" class="btn btn-info btn-sm"> Edit </a><br><br>
                             <form action="/karyawan/{{$table->id}}" method="post">
                              @csrf
                              @method('DELETE')
                              <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>

                      </tr>
                  @endforeach
                </tbody>          
                </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  @endsection
