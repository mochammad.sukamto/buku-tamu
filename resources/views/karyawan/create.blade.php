@extends('adminlte.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Form Input Karyawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">General Form</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
<form role="form" action="/karyawan" method="GET">
    <div class="card-body">
      <div class="form-group">
        <label for="Nama">Nama</label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
      </div>
      <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="Enter ...">
      </div>
      <div class="form-group">
        <label for="No Telpon">No Telpon</label>
        <input type="text" class="form-control" name="no_telpon" id="no_telpon" placeholder="No Telpon">
      </div>
      <div class="form-group">
        <label>Alamat</label>
        <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Enter ..."></textarea>
      </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>


@endsection
