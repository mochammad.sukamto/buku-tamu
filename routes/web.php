<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/formTamu', function () {
    return view('formTamu');
});

Route::get('/formKaryawan', function () {
    return view('formKaryawan');
});

Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/items', function () {
    return view('items.index');
});

//tamu
Route::get('/tamu/create','tamuController@createTamu');

Route::get('/tamu','tamuController@storeTamu');

Route::get('/tamu/table','tamuController@tableTamu');

Route::get('/tamu/edit/{id}','tamuController@editTamu');

Route::get('/tamu/update/{id}','tamuController@updateTamu');

Route::delete('/tamu/{id}','tamuController@deleteTamu');

Route::get('/exporttamu','tamuController@exportTamu')->name('exporttamu');

//karyawan

Route::get('/karyawan/create','karyawanController@createKaryawan');

Route::get('/karyawan','karyawanController@storeKaryawan');

Route::get('/karyawan/table','karyawanController@tableKaryawan');

Route::get('/karyawan/edit{id}','karyawanController@editKaryawan');

