<?php

namespace App\Exports;

use App\tamu;
use Maatwebsite\Excel\Concerns\FromCollection;

class exportTamu implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return tamu::all();
    }
}
