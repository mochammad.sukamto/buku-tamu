<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\karyawan;
use DB;

class karyawanController extends Controller
{
    public function createKaryawan(){
        return view('karyawan.create');
    }

    public function storeKaryawan(Request $request){
        // $data = new karyawan();
        // $data->name = $request->name;
        // $data->password = $request->password;
        // $data->no_telpon = $request->no_telpon;
        // $data->alamat = $request->alamat;
        // $data->save();
        // return redirect('karyawan/create');
        
         $query = DB::table('form_karyawan')->insert([
            "name" => $request["name"],
            "no_telpon" => $request["no_telpon"],
            "password" => $request["password"],
            "alamat" => $request["alamat"],
        ]);
        return redirect('karyawan/table ');
    }

    public function tableKaryawan(){
        $table = DB::table('form_karyawan')->get();
        return view('karyawan.table',compact('table'));
        
    }

    public function editTamu($id){
        $data = DB::table('form_karyawan')->where('id', $id)->get();
        return view('karyawan.edit', compact('data'));
        
    }




}
