<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\tamu;
use App\Export\exportTamu;
use Maatwebsite\Excel\Facedes\Excel;
use App\Http\Controllers\Controller;

use DB;

class tamuController extends Controller

{
    public function exportTamu(){
        return Excel::download(new exportTamu,'tamu.xlsx');
    }



    public function createTamu(){
        return view('tamu.create');
        
    }

    public function storeTamu(Request $request){
        // $query = DB::table('form_tamu')->insert([
        //     "name" => $request["name"],
        //     "no_telpon" => $request["no_telpon"],
        //     "maksud_tujuan" => $request["maksud_tujuan"],
        //     "alamat" => $request["alamat"],
        //     "tanggal_datang" => $request["tanggal_datang"],
        //     "tanggal_keluar" => $request["tanggal_keluar"],
        //     "barang_titip" => $request["barang_titip"],
        // ]);
        
        $data = new tamu();
        $data->name = $request->name;
        $data->no_telpon = $request->no_telpon;
        $data->maksud_tujuan = $request->maksud_tujuan;
        $data->alamat = $request->alamat;
        $data->tanggal_datang = $request->tanggal_datang;
        $data->tanggal_keluar = $request->tanggal_keluar;
        $data->barang_titip = $request->barang_titip;
        $data->save();
        return redirect('tamu/table');
    }


    public function tableTamu(){
        $table = DB::table('form_tamu')->OrderBy('updated_at','DESC')->OrderBy('created_at','DESC')->get();
        return view('tamu.table', compact('table'));
        
    }

    public function editTamu($id){
        $data = DB::table('form_tamu')->where('id', $id)->get();
        return view('tamu.edit', compact('data'));
        
    }

    public function updatetamu(Request $request, $id)
    {
        $data = tamu::where('id',$id)->first();
        $data->name = $request->name;
        $data->no_telpon = $request->no_telpon;
        $data->maksud_tujuan = $request->maksud_tujuan;
        $data->alamat = $request->alamat;
        $data->tanggal_datang = $request->tanggal_datang;
        $data->tanggal_keluar = $request->tanggal_keluar;
        $data->barang_titip = $request->barang_titip;
        $data->save();
        return redirect('tamu/table')->with('success','Data saved !!');
        
    }

    public function deleteTamu($id){
        $query = DB :: table('form_tamu')->where('id',$id)->delete();
        return redirect('tamu/table');
    }

}

    